﻿
namespace QueryDecorator
{
    public abstract class AbstractQueryDecorator : AbstractQuery
    {
        private readonly AbstractQuery _abstractQuery;
    
        protected string Sql = "Undefined Decorator";

        protected AbstractQueryDecorator(AbstractQuery abstractQuery)
        {
            _abstractQuery = abstractQuery;
        }

        public override string GetSql()
        {
            return string.Format("{0} {1}", _abstractQuery.GetSql(), Sql);
        }
    }
}

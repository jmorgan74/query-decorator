﻿using System;

namespace QueryDecorator
{
    class Program
    {
        static void Main(string[] args)
        {
            var query = new Query();

            Console.Out.WriteLine(query.GetSql());

            var andQueryDecorator = new NameQueryDecorator(query);

            Console.Out.WriteLine(andQueryDecorator.GetSql());

            Console.ReadKey();
        }
    }
}

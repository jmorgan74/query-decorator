﻿
namespace QueryDecorator
{
    class Query : AbstractQuery
    {
        const string Sql = "SELECT * FROM allTables WHERE True = True";

        public override string GetSql()
        {
            return Sql;
        }
    }
}

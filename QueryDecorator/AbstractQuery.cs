﻿
namespace QueryDecorator
{
    public abstract class AbstractQuery
    {
        public abstract string GetSql();
    }
}

﻿
namespace QueryDecorator
{
    public class NameQueryDecorator : AbstractQueryDecorator
    {
        public NameQueryDecorator(AbstractQuery abstractQuery)
            : base(abstractQuery)
        {
            Sql = "AND Name = 'Badger'";
        }
    }
}
